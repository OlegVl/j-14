package ru.ovk.home.controller;
import java.util.*;

public class CommandHistoryController {
    private int COMMAND_COUNT = 10;

    private HashSet<String> trueCommand = new HashSet<>(); //тут список команд, которые можно сохранять в истории

    private final Deque<String> historyCmd = new ArrayDeque<>(COMMAND_COUNT);


    public CommandHistoryController() {
        trueCommand.add("help");
        trueCommand.add("version");
        trueCommand.add("error");
        //...  и так все команды из TerminalConst
        //нужно найти как из класса получить список членов типа String и получить эти значения и засунуть в HashSet trueCommand
    }


    private Boolean testForSave(final String command){
        if( command==null || command.isEmpty() || !historyCmd.contains(command) ) return false;
        else return true;
    }

    public int addInHistory(final String command) {
      //  if(!testForSave(command)) return 0;
        if (historyCmd.size() == COMMAND_COUNT) historyCmd.removeFirst();
        historyCmd.addLast(command);
        return 0;
    }

    public int displayHistory() {
        ArrayList<String> commandList = new ArrayList<String>(historyCmd);
        for (int i = 0; i < commandList.size(); i++) {
            System.out.println("# " + (i+1) + ": " + commandList.get(i));
        }
        return 0;
    }




}
