package ru.ovk.home.repository;
import ru.ovk.home.entity.Project;


import java.util.*;

public class ProjectRepository {
    private List<Project> projects = new ArrayList<>();
    private final HashMap<String, List<Project>> projectsIndexName = new HashMap<>();

    private void deleteLinkInIndexName(String name, Long id) {
        List<Project> projectsList = projectsIndexName.get(name);
        for(Project project: projectsList) {
          if(project.getId()==id) projectsList.remove(project);
        }
    }


    public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        List<Project> projectsList = projectsIndexName.get(project.getName());
        if (projectsList == null) projectsList = new ArrayList<>();
        projectsList.add(project);
        projectsIndexName.put(project.getName(), projectsList);

        return project;
    }

    public void clear() {
        projects.clear();
        projectsIndexName.clear();
    }

    public List<Project> findAll() {
        return projects;
    }

    public Project findByIndex(final int index) {
        return projects.get(index);
    }

    public Project findByName(final String name) {
        final List<Project> projectList = projectsIndexName.get(name);
        for(final Project project: projectList) {
            if(project!=null) return project;
        }
        return null;
    }
    public Project findById(final Long id) {
        for (final Project project : projects) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public Project removeByIndex(final int id) {
        final Project project = findByIndex(id);
        if(project==null) return null;

        projects.remove(project);
        deleteLinkInIndexName(project.getName(), project.getId());
        return project;
    }

    public Project removeById(final Long id) {
        final Project project = findById(id);
        if(project==null) return null;

        deleteLinkInIndexName(project.getName(), project.getId());
        projects.remove(project);
        return project;
    }

    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if(project==null) return null;

        deleteLinkInIndexName(project.getName(), project.getId());
        projects.remove(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description) {
        Project project = findById(id);
        if(project==null) return  null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }
}