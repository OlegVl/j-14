package ru.ovk.home.repository;
import ru.ovk.home.entity.Project;
import ru.ovk.home.entity.Task;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TaskRepository {
    private List<Task> tasks = new ArrayList<>();
    private final HashMap<String, List<Task>> TasksIndexName = new HashMap<>();

    private void deleteLinkInIndexName(String name, Long id) {
        List<Task> tasksList = TasksIndexName.get(name);
        for(Task task: tasksList) {
            if(task.getId()==id) tasksList.remove(task);
        }
    }

    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    public Task create(final String name, final String description) {
        final Task task = new Task(name);
        tasks.add(task);

        List<Task> tasksList = TasksIndexName.get(task.getName());
        if (tasksList == null) tasksList = new ArrayList<>();
        tasksList.add(task);
        TasksIndexName.put(task.getName(), tasksList);

        return task;
    }


    public void clear() {
        tasks.clear();
        TasksIndexName.clear();
    }
    public List<Task> findAll() {
        return tasks;
    }

    public Task findByIndex(final int index) {
        if(tasks.size()-1 < index) return null;
        return tasks.get(index);
    }

    public Task findByName(final String name) {
        final List<Task> tasksList = TasksIndexName.get(name);
        for(final Task task: tasksList) {
            if(task!=null) return task;
        }
        return null;
    }

    public Task findById(final Long id) {
        for(final Task task: tasks) {
            if(task.getId().equals(id)) return task;
        }
        return null;
    }
    public Task removeByIndex(final int id) {
        final Task task = findByIndex(id);
        if(task==null) return null;
        tasks.remove(task);

        deleteLinkInIndexName(task.getName(), task.getId());
        return task;
    }
    public Task removeById(final Long id) {
        final Task task = findById(id);
        if(task==null) return null;
        tasks.remove(task);

        deleteLinkInIndexName(task.getName(), task.getId());
        return task;
    }
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if(task==null) return null;
        tasks.remove(task);
        deleteLinkInIndexName(task.getName(), task.getId());
        return task;
    }

    public Task update(final Long id, final String name, final String description) {
        Task task = findById(id);
        if(task==null) return  null;
        task.setName(name);
        task.setDescription(description);

        return task;
    }

        public List<Task> findAddByProjectId(final Long projectId) {
            final List<Task> result = new ArrayList<>();
            for (final Task task: findAll()) {
                final Long idProject = task.getProjectId();
                if (idProject == null) continue;
                if (idProject.equals(projectId)) result.add(task);
            }
            return result;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (id == null) return null;
        for (final Task task: tasks ) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }


}